//
//  HomePageCollectionViewCell.h
//  Electronicsignature
//
//  Created by wangyabo on 16/3/29.
//  Copyright © 2016年 mahao. All rights reserved.
//

#import <UIKit/UIKit.h>
// 首页 12个图标的自定义cell
@interface HomePageCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) UIImageView *picImageView; //背景图片
@property (nonatomic, strong) UILabel *bottomTitle; //底部的文字
@property (nonatomic, strong) UILabel *numberLab; // 右上角显示的数字
@property (nonatomic, strong) UIImageView *numberImage;
@end
