//
//  main.m
//  横向滑动Collection
//
//  Created by zhangyuanjie on 16/11/28.
//  Copyright © 2016年 asingers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
