//
//  MyCollectionViewLayout.h
//  横向滑动Collection
//
//  Created by zhangyuanjie on 16/11/28.
//  Copyright © 2016年 asingers. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectionViewLayout : UICollectionViewLayout
@property (nonatomic) CGFloat minimumLineSpacing; //行间距

@property (nonatomic) CGFloat minimumInteritemSpacing; //item间距

@property (nonatomic) CGSize itemSize; //item大小

@property (nonatomic) UIEdgeInsets sectionInset;

- (instancetype)init;
@end
