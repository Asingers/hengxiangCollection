//
//  ViewController.m
//  横向滑动Collection
//
//  Created by zhangyuanjie on 16/11/28.
//  Copyright © 2016年 asingers. All rights reserved.
//
//有状态栏时的屏幕宽度
#define IISCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_WIDTH self.bounds.size.width
//无状态栏时的屏幕高度
#define IISCREEN_HEIGH ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGH self.bounds.size.height
//有状态栏时的屏幕高度
#define IISCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height - 20)
#import "ViewController.h"
#import "MyCollectionViewLayout.h"
#import "HomePageCollectionViewCell.h"
@interface ViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic, strong) NSArray *data; //数据源
@property (nonatomic,strong) NSArray *oldData; // 原始数据
@property (nonatomic,strong) NSMutableArray *MyNewData;
@property (nonatomic,strong) UICollectionView *myCollection;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    MyCollectionViewLayout *layout = [[MyCollectionViewLayout alloc]init];
    layout.sectionInset = UIEdgeInsetsMake(5, 10, 5, 10);
    layout.minimumLineSpacing = 10;
    layout.minimumInteritemSpacing = 1;
    layout.itemSize = CGSizeMake((IISCREEN_WIDTH-60)/4, 50);
    
    _MyNewData = [[NSMutableArray alloc]init];
    NSString *a = @"快";
    NSString *b = @"新";
    NSString *c = @"乐";
    NSString *d = @"年";
    
    NSMutableDictionary *oldDic = [[NSMutableDictionary alloc]init];
    [oldDic setObject:a forKey:@"3"];
    [oldDic setObject:b forKey:@"1"];
    [oldDic setObject:c forKey:@"4"];
    [oldDic setObject:d forKey:@"2"];
    _oldData = @[a,b,c,d];

    NSArray *arr = oldDic.allKeys;
    
    NSArray *newArr = [arr sortedArrayUsingSelector:@selector(compare:)];


    for (NSString *str in newArr) {
    
        [_MyNewData addObject:[oldDic objectForKey:str]];
    }
    
    self.myCollection = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 64, IISCREEN_WIDTH, 150) collectionViewLayout:layout];
    self.myCollection.backgroundColor = [UIColor clearColor];
    self.myCollection.tag = 100;
    self.myCollection.delegate = self;
    self.myCollection.dataSource = self;
    self.myCollection.showsVerticalScrollIndicator = NO;
    self.myCollection.showsHorizontalScrollIndicator = NO;
    self.myCollection.pagingEnabled = YES;
    self.myCollection.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.3];
    [_myCollection registerClass:[HomePageCollectionViewCell class] forCellWithReuseIdentifier:@"aaa"];
    [self.view addSubview:self.myCollection];

}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
 
    static NSString *inden = @"aaa";
    
    HomePageCollectionViewCell *cell = (HomePageCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:inden forIndexPath:indexPath];
    NSString *name;
    name = [_MyNewData objectAtIndex:indexPath.item];
    cell.bottomTitle.text = name;
    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _MyNewData.count;
    
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    HomePageCollectionViewCell *cell = (HomePageCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    NSString *msg = cell.bottomTitle.text;
    NSLog(@"%@",msg);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
