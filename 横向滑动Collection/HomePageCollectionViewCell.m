//
//  HomePageCollectionViewCell.m
//  Electronicsignature
//
//  Created by wangyabo on 16/3/29.
//  Copyright © 2016年 mahao. All rights reserved.
//
#import "UIView+Frame.h"
//有状态栏时的屏幕宽度
#define IISCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_WIDTH self.bounds.size.width
//无状态栏时的屏幕高度
#define IISCREEN_HEIGH ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_HEIGH self.bounds.size.height
//有状态栏时的屏幕高度
#define IISCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height - 20)
#import "HomePageCollectionViewCell.h"

@implementation HomePageCollectionViewCell

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
       self.picImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height - 20)];
        self.picImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        self.picImageView.contentMode = UIViewContentModeScaleAspectFit;
        //self.picImageView.backgroundColor = [UIColor whiteColor];
        NSInteger distance = 0;
        if (IISCREEN_WIDTH == 414) {
            distance = 24;
        }else {
            distance = 24;
        }
        self.numberImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.width - distance, 0, 20, 20)];
        //self.numberImage.backgroundColor = [UIColor blueColor];
        self.numberImage.image = [UIImage imageNamed:@"Oval"];
        self.numberImage.hidden = NO;
        self.numberLab = [[UILabel alloc] initWithFrame:CGRectMake(3, 2.5, 15, 15)];
        self.numberLab.textAlignment = NSTextAlignmentCenter;
        self.numberLab.font = [UIFont systemFontOfSize:8.0f];
        self.numberLab.backgroundColor = [UIColor clearColor];
        self.numberLab.textColor = [UIColor whiteColor];
        //[self.numberLab sizeToFit];
        [self.picImageView addSubview:self.numberImage];
        [self.numberImage addSubview:self.numberLab];
        [self addSubview:self.picImageView];
        self.bottomTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - 13 , self.bounds.size.width , 15)];
        self.bottomTitle.backgroundColor = [UIColor clearColor];
        self.bottomTitle.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.bottomTitle];

   
    
    }
    return self;
}

@end
